import type { IUser } from "@/interfaces/IUser";
import { ref } from 'vue' // función de reactividad
import type { Ref } from 'vue' // La interfaz de reactividad (ref)
const url = "http://172.16.107.120:3000" //"172.16.107.120:3000" // 192.168.137.152:3000

export default class UserService {
    private users: Ref<IUser[]>;
    private user: Ref<IUser>

    constructor() {
        this.users = ref([])
        this.user = ref({}) as Ref<IUser>
    }
    //Son Los getters
    getUsers(): Ref<IUser[]> {
        return this.users
    }
    getUser(): Ref<IUser> {
        return this.user
    }

    async fetchAll(): Promise<void> {
        try {
            const json = await fetch(url + '/Users');
            const response = await json.json()
            this.users.value = await response;
        } catch (error) {
            console.log(error)
        }
    }

    async fetchUser(email: string): Promise<void> {
        try {
            const json = await fetch(url + '/User/' + email)
            const response = await json.json()
            this.user.value = await response
        } catch (error) {
            console.log(error)
        }
    }
    async fetchPost(name: string, email: string, password: string): Promise<void> {
        try {
            const json = await fetch(url + '/register?' + 'name=' + name + '&email=' + email + '&password=' + password)
            const response = await json.json()
            console.log(response);

        } catch (error) {
            console.log(error)
        }
    }
}